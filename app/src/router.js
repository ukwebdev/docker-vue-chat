import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const Login = () => import('./components/pages/Login.vue');
const Register = () => import('./components/pages/Register.vue');
const Logout = () => import('./components/pages/Logout.vue');
const PageNotFoundPage = () => import('./components/pages/PageNotFoundPage.vue');

import Home from './components/pages/Home.vue';
import Products from './components/pages/Products.vue';

import Account from './components/pages/Account.vue';

const router = new Router({
  mode: 'history',
  scrollBehavior: () =>
    setTimeout(() => {
      y: 0;
    }, 300),
  routes: [
    {
      path: '/login',
      component: Login,
      meta: {
        title: 'Login'
      }
    },
    {
      path: '/register',
      component: Register,
      meta: {
        title: 'Register'
      }
    },
    {
      path: '/logout',
      component: Logout,
      meta: {
        title: 'Logout'
      }
    },
    {
      path: '/',
      component: Home,
      meta: {
        title: 'Home',
        requiresAuth: true
      }
    },
    {
      path: '/products',
      component: Products,
      meta: {
        title: 'Products',
        requiresAuth: true
      }
    },
    {
      path: '/account',
      component: Account,
      meta: {
        title: 'Account',
        requiresAuth: true
      }
    },
    {
      path: '/404',
      component: PageNotFoundPage,
      meta: {
        title: '404 Not Found'
      }
    },
    {
      path: '*',
      redirect: '/404'
    }
  ]
});

router.beforeEach((to, from, next) => {
  const isTokenExist = !!localStorage.getItem('token');

  document.title = `Demo | ${to.meta.title}`;

  if (to.matched.some(record => record.meta.requiresAuth) && !isTokenExist) {
    return next({
      path: '/login',
      params: { nextUrl: to.fullPath }
    });
  } else if ((to.path === '/login' || to.path === '/register') && isTokenExist) {
    return next({ path: '/' });
  }

  return next();
});

export default router;
