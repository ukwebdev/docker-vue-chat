import api from '../../api';
import * as types from '../mutation-types';
import { getProductsList } from '../../api';

const state = {
  products: [],
  loading: false
};

const getters = {
  getProducts: state => state.products,
  isProductsLoading: state => state.loading
};

const actions = {
  async fetchProducts({ commit }) {
    commit(types.PRODUCTS_SET_STATUS_LOADING, true);

    try {
      const products = await api.getProductsList();
      commit(types.PRODUCTS_UPDATE, products);
    } catch (error) {
      return Promise.reject(error);
    }

    commit(types.PRODUCTS_SET_STATUS_LOADING, false);
  }
};

const mutations = {
  [types.PRODUCTS_UPDATE](state, products) {
    state.products = products;
  },
  [types.PRODUCTS_SET_STATUS_LOADING](state, bool) {
    state.loading = bool;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
