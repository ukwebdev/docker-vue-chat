import Vue from 'vue';
import Vuex from 'vuex';

import AuthModule from './modules/auth';
import UsersModule from './modules/users';
import MessagesModule from './modules/messages';
import ProductsModule from './modules/products';

import * as types from './mutation-types';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    AuthModule,
    UsersModule,
    MessagesModule,
    ProductsModule
  }
});
