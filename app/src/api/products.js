import { api } from './config';

export const getProductsList = async () => {
  try {
    const { data } = await api.get('/products');
    return data;
  } catch (error) {
    if (error.response.status === 401) {
      throw new Error('Unauthorized');
      return;
    }
    throw error;
  }
};
