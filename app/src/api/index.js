import * as auth from './auth';
import * as users from './users';
import * as messages from './messages';
import * as products from './products';

export default {
  ...auth,
  ...users,
  ...messages,
  ...products
};
