const User = require('../models/user');

const getList = async ctx => {
  try {
    let users = await User.find({});

    ctx.body = users.map(user => {
      return user.toJSON();
    });
  } catch (error) {
    ctx.status = 400;
    ctx.body = error;
  }
};

const updateById = async ctx => {
  try {
    const userId = ctx.params.id;
    const body = ctx.request.body;

    const update = {
      firstName: body.firstName,
      lastName: body.lastName
    };

    const user = await User.findByIdAndUpdate(userId, update, { new: true });

    ctx.body = user;
  } catch (error) {
    ctx.status = 400;
    ctx.body = error;
  }
};

const getById = async ctx => {
  try {
    const user = await User.findById(ctx.params.id);

    ctx.body = user.toJSON();
  } catch (error) {
    ctx.status = 400;
    ctx.body = error;
  }
};

const getCurrent = async ctx => {
  ctx.body = ctx.req.user;
};

module.exports = {
  getList,
  getById,
  getCurrent,
  updateById
};
