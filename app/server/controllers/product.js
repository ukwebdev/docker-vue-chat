const fs = require('fs');
const path = require('path');

const mockPath = path.resolve(__dirname, '../mock/products.json');

const getList = async ctx => {
  try {
    let rawData = fs.readFileSync(mockPath);
    ctx.body = JSON.parse(rawData);
  } catch (error) {
    console.log(error);
    ctx.status = 400;
    ctx.body = error;
  }
};

module.exports = {
  getList
};
