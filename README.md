# Demo App

---

### Development

```sh
$ git clone {git_repo_url}
$ cd app
$ docker-compose -f docker-compose.dev.yml up -d
```

Then navigate to http://localhost

### Production

```sh
$ git clone {git_repo_url}
$ cd app
$ docker-compose up -d
```

### TODOs:

- [ ] Messages pagination (lazy-loading on scroll) to prevent the entire list of messages from loading
- [ ] Replace messages list scroll with some library something like [this one](https://github.com/Degfy/vue-perfect-scrollbar)
- [ ] Account page.
